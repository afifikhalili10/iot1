import 'package:flutter/material.dart';

//hanya edit nama tajuk atas sahaja
class ScreenTitle extends StatelessWidget {
  final String text;

  const ScreenTitle({Key key, this.text}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Text(
      text,
      style: TextStyle(
          fontSize: 32, color: Colors.black, fontWeight: FontWeight.bold),
    );
  }
}
