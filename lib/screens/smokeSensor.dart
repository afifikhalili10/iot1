import 'dart:async';
import 'package:flutter/material.dart';
import 'package:firebase_database/firebase_database.dart';
//import 'package:flutter/services.dart';

import 'CircleProgress.dart';

class smokeSensor extends StatefulWidget {
  @override
  _smokeSensorState createState() => _smokeSensorState();
}

class _smokeSensorState extends State<smokeSensor>
    with SingleTickerProviderStateMixin {
  //
  final dbRef = FirebaseDatabase.instance.reference();
  bool value = false;
  bool isLoading = false;

  onUpdate() {
    setState(() {
      value = !value;
    });
  }

  //Animation
  AnimationController progressController;
  Animation<double> tempAnimation;
  Animation<double> humidityAnimation;

  @override

  //

  //
  Widget build(BuildContext context) {
    return Scaffold(
      //Wallpaper Background

      body: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage("assets/images/bluelightGradient.jpg"),
            fit: BoxFit.cover,
          ),
        ),
        child: SafeArea(
          child: StreamBuilder(
              builder: (context, snapshot) {
                if (snapshot.hasData &&
                    !snapshot.hasError &&
                    snapshot.data.snapshot.value != null) {
                  //
                  return Center(
                    child: Column(
                      children: [
                        // Box Empty Space
                        SizedBox(
                          height: 50,
                          child: Container(
                              // color: Colors.red,
                              ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(18.0),
                          child: FloatingActionButton.extended(
                            icon: value
                                ? Icon(Icons.visibility)
                                : Icon(Icons.visibility_off),
                            backgroundColor: value ? Colors.yellow : Colors.red,
                            label: value ? Text("ON") : Text("OFF"),
                            elevation: 20.00,
                            onPressed: () {
                              onUpdate();
                              writeData();
                            },
                          ),
                        ),
                      ],
                    ),
                  );
                } else {}
                return Container();
              },
              stream: dbRef.child("Data").onValue),
        ),
      ),
    );
  }

  Future<void> writeData() async {
    dbRef.child("LightFireAlarm").set({"switch": !value});
  }
}
