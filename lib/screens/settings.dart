import 'package:flutter/material.dart';
import 'package:flutter_switch/flutter_switch.dart';

class Settings extends StatefulWidget {
  @override
  _SettingsState createState() => _SettingsState();
}

class _SettingsState extends State<Settings> {
  bool status = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.brown[400],
          title: Text('Settings'),
          leading: IconButton(
            onPressed: () {},
            icon: Icon(Icons.brightness_3),
            color: Colors.black,
          ),
        ),
        body: Center(
          child: Container(
            child: Text('Dark Mode'),
            // child: FlutterSwitch(
            //   width: 125.0,
            //   height: 55.0,
            //   valueFontSize: 25.0,
            //   toggleSize: 45.0,
            //   value: status,
            //   borderRadius: 30.0,
            //   padding: 8.0,
            //   showOnOff: true,
            //   onToggle: (val) {
            //     setState(() {
            //       status = val;
            //     });
            //   },
            // ),
          ),
        ));
  }
}
