import 'package:flutter/material.dart';
import 'package:iot_home_app/screens/screens.dart';

class BottomNavsScreen extends StatefulWidget {
  @override
  _BottomNavsScreenState createState() => _BottomNavsScreenState();
}

class _BottomNavsScreenState extends State<BottomNavsScreen> {
  final List _screens = [
    HomePage(),
    Dashboard(),
    Settings(),
    TempSensor(),
  ];

  int _currentIndex = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _screens[_currentIndex],
      bottomNavigationBar: BottomNavigationBar(
        currentIndex: _currentIndex,
        onTap: (index) => setState(() => _currentIndex = index),
        items: [
          BottomNavigationBarItem(
              icon: Icon(Icons.home),
              label: 'Home',
              backgroundColor: Colors.blue),
          BottomNavigationBarItem(
              icon: Icon(Icons.dashboard),
              label: 'Dashboard',
              backgroundColor: Colors.red[300]),
          BottomNavigationBarItem(
              icon: Icon(Icons.settings),
              label: 'Settings',
              backgroundColor: Colors.grey),
          BottomNavigationBarItem(
              icon: Icon(Icons.settings),
              label: 'Settings',
              backgroundColor: Colors.grey),
        ],
      ),
    );
  }
}
