import 'dart:async';
import 'package:flutter/material.dart';

class TempSensor2 extends StatefulWidget {
  @override
  _TempSensor2State createState() => _TempSensor2State();
}

class _TempSensor2State extends State<TempSensor2> {
  @override
  // final dbRef = FirebaseDatabase.instance.reference();
  //bool value = false;
  Widget build(BuildContext context) {
    return Scaffold(
      //Wallpaper Background

      body: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage("assets/images/bluelightGradient.jpg"),
            fit: BoxFit.cover,
          ),
        ),
        child: SafeArea(
          //child: StreamBuilder(
          // builder: (context, snapshot) {
          //   if (snapshot.hasData &&
          //       !snapshot.hasError &&
          //       snapshot.data.snapshot.value != null) {
          child: Column(
            children: [
              Padding(
                padding: EdgeInsets.all(20),
                child: Row(
                  //bahagian Row Atas 2 icon dan nama
                  //spaceEvenly middle
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    // Icon(
                    //   Icons.clear_all,
                    //   color: Colors.black,
                    // ),

                    //Name Area
                    Text(
                      "MyRoom",
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 20,
                          fontWeight: FontWeight.bold),
                    ),

                    // Icon(Icons.settings)
                  ],
                ),
              ),

              // Box Empty Space
              SizedBox(
                height: 50,
                child: Container(
                    // color: Colors.red,
                    ),
              ),

              //temp value
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Column(
                    children: [
                      Padding(
                        padding: const EdgeInsets.all(8.0),

                        //name temp
                        child: Text(
                          "Temperature", //
                          style: TextStyle(
                              fontSize: 20, fontWeight: FontWeight.bold),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Text(
                          // snapshot.data.snapshot.value["Temperature:"]
                          //                     .toString() +
                          //                 "°C",

                          "20", // LETAK VALUE suhu
                          style: TextStyle(
                              fontSize: 20, fontWeight: FontWeight.bold),
                        ),
                      ),
                    ],
                  )
                ],
              ),

              // Box Empty Space
              SizedBox(
                height: 50,
                child: Container(
                    // color: Colors.red,
                    ),
              ),

              Column(
                children: [
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(
                      "Humidity",
                      style:
                          TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(
                      "20", // LETAK VALUE humidity
                      // snapshot.data.snapshot.value["Humidity:"]
                      //                     .toString() +
                      //                 "%",

                      //
                      style:
                          TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                    ),
                  ),
                ],
              ),
            ],
          ),
          //   } else {}
          //   return Container();
          // },
        ),
      ),
      //)
    );
  }

  //
  //   Future<void> readData() async {
  //   dbRef.child("Data").once().then((DataSnapshot snapshot) {
  //     print(snapshot.value);
  //   });
  // }
}
