import 'package:flutter/material.dart';
import 'package:iot_home_app/models/ColList.dart';

class Details extends StatelessWidget {
  final ColList list;
  Details({@required this.list});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0,
      ),
      extendBodyBehindAppBar: true,
      body: Container(
          child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          //ClipRect

          SizedBox(height: 20),
          ListTile(
            title: Text(list.title,
                style: TextStyle(
                    // color blkang nama
                    backgroundColor: Colors.brown[400],
                    fontWeight: FontWeight.bold,
                    fontSize: 20,
                    color: Colors.grey[800])),
            subtitle: Text(
              '${list.title} nama title \$${list.title}',
              style: TextStyle(letterSpacing: 1),
            ),
          ),
        ],
      )),
    );
  }
}
