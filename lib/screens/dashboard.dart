import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
//import 'package:iot_home_app/screens/RowColumnList.dart';
import 'package:iot_home_app/screens/temp_details.dart';
import 'package:iot_home_app/screens/humidity_details.dart';
import 'package:iot_home_app/screens/smokeSensor.dart';
import 'package:iot_home_app/screens/tableData.dart';

class Dashboard extends StatefulWidget {
  @override
  _DashboardState createState() => _DashboardState();
}

class _DashboardState extends State<Dashboard> {
  @override
  Widget build(BuildContext context) {
    //create digital clock
    var now = DateTime.now();
    var formattedTime = DateFormat('HH:mm').format(now);
    var formattedDate = DateFormat('EEE, d MM').format(now);

    return Scaffold(
      //pada bar di atas
      appBar: AppBar(
        centerTitle: true,
        title: Text('Dashboard'),
      ),

      //buat spacing letak gambar dan automation di atas
      body: SafeArea(
        child: Container(
          padding: EdgeInsets.all(10.0), //size cortainer overall

          //background wallpaper
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage("assets/images/bluelightGradient.jpg"),
              fit: BoxFit.cover,
            ),
          ),

          //jam dan background column
          child: Column(
            children: <Widget>[
              SizedBox(height: 5),
              Container(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: <Widget>[
                      Text(
                        formattedTime,
                        style: TextStyle(color: Colors.white, fontSize: 64),
                      ),
                      Text(
                        formattedDate,
                        style: TextStyle(color: Colors.white, fontSize: 20),
                      ),
                    ],
                  ),
                  height: 150,
                  decoration: BoxDecoration(
                    border: Border.all(
                      width: 8,
                      color: Colors.red,
                    ),
                    borderRadius: BorderRadius.circular(10),
                  )),

              //Jarak

              Container(
                padding: EdgeInsets.all(10.0),
                child: Column(
                  children: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: <Widget>[
                        Container(
                          child: GestureDetector(
                            onTap: () {
                              Navigator.push(
                                  context,
                                  //trip => list
                                  MaterialPageRoute(
                                      builder: (context) => TempSensor()));
                            },
                            //
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              children: <Widget>[
                                SizedBox(
                                  child: Image.asset(
                                    'assets/images/TempIcon.jpg',
                                    height: 150,
                                    width: 150,
                                    fit: BoxFit.fitHeight,
                                  ),
                                  height: 150,
                                  width: 150,
                                ),
                              ],
                            ),
                          ),
                        ),

                        //
                        Container(
                            child: GestureDetector(
                          onTap: () {
                            Navigator.push(
                                context,
                                //trip => list
                                MaterialPageRoute(
                                    builder: (context) => HumidSensor()));
                          },
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: <Widget>[
                              SizedBox(
                                child: Image.asset(
                                  'assets/images/humidity.png',
                                  height: 150,
                                  width: 150,
                                  fit: BoxFit.fitHeight,
                                ),
                              ),
                            ],
                          ),
                        )),
                      ],
                    ),
                  ],
                ),
              ),
              Container(
                padding: EdgeInsets.all(10.0),
                // decoration: BoxDecoration(
                //     //Padding LineBorder
                //     border: Border.all(
                //   width: 8,
                //   color: Colors.red,
                // )),

                //child: RowColumnList(),
                child: Column(
                  children: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: <Widget>[
                        Container(
                          child: GestureDetector(
                            onTap: () {
                              Navigator.push(
                                  context,
                                  //trip => list
                                  MaterialPageRoute(
                                      builder: (context) => smokeSensor()));
                            },
                            //
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              children: <Widget>[
                                SizedBox(
                                  child: Image.asset(
                                    'assets/images/SmokeSensor.jpg',
                                    height: 150,
                                    width: 150,
                                    fit: BoxFit.fitHeight,
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),

                        //
                        Container(
                            child: GestureDetector(
                          onTap: () {
                            Navigator.push(
                                context,
                                //trip => list
                                MaterialPageRoute(
                                    builder: (context) => TableData()));
                          },
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: <Widget>[
                              SizedBox(
                                child: Image.asset(
                                  'assets/images/table_icon.png',
                                  height: 150,
                                  width: 150,
                                  fit: BoxFit.fitHeight,
                                ),
                              )
                            ],
                          ),
                        )),
                      ],
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
