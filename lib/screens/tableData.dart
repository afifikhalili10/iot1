import 'dart:async';
import 'package:intl/intl.dart';
import 'package:flutter/material.dart';
import 'package:firebase_database/firebase_database.dart';
//import 'package:flutter/services.dart';

class TableData extends StatefulWidget {
  @override
  _TableDataState createState() => _TableDataState();
}

class _TableDataState extends State<TableData> {
  @override
  final dbRef = FirebaseDatabase.instance.reference();
  //bool value = false;
  Widget build(BuildContext context) {
    var now = DateTime.now();
    var formattedTime = DateFormat('HH:mm').format(now);
    var formattedDate = DateFormat('EEE, d MM').format(now);

    return Scaffold(
      //Wallpaper Background

      body: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage("assets/images/bluelightGradient.jpg"),
            fit: BoxFit.cover,
          ),
        ),
        child: SafeArea(
          child: StreamBuilder(
              builder: (context, snapshot) {
                if (snapshot.hasData &&
                    !snapshot.hasError &&
                    snapshot.data.snapshot.value != null) {
                  //
                  return Center(
                    child: Column(
                      children: [
                        Padding(
                          padding: EdgeInsets.all(20),
                          child: Row(
                            //
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              //Name Area
                              Text(
                                "Table Data",
                                style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 20,
                                    fontWeight: FontWeight.bold),
                              ),

                              // Icon(Icons.settings)
                            ],
                          ),
                        ),

                        // Box Empty Space
                        SizedBox(
                          height: 50,
                          child: Container(
                              // color: Colors.red,
                              ),
                        ),

                        //temp value

                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Column(
                              children: [
                                Padding(
                                  padding: const EdgeInsets.all(8.0),

                                  //name temp
                                  child: Text(
                                    "Temperature", //
                                    style: TextStyle(
                                        fontSize: 20,
                                        fontWeight: FontWeight.bold),
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Text(
                                    snapshot.data.snapshot.value["Temperature"]
                                            .toString() +
                                        " °C",

                                    // LETAK VALUE suhu
                                    style: TextStyle(
                                        fontSize: 20,
                                        fontWeight: FontWeight.bold),
                                  ),
                                ),
                              ],
                            )
                          ],
                        ),

                        // Box Empty Space
                        SizedBox(
                          height: 50,
                          child: Container(
                              // color: Colors.red,
                              ),
                        ),

                        Column(
                          children: [
                            Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Text(
                                "Humidity",
                                style: TextStyle(
                                    fontSize: 20, fontWeight: FontWeight.bold),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Text(
                                // LETAK VALUE humidity
                                snapshot.data.snapshot.value["Humidity"]
                                        .toString() +
                                    " %",

                                style: TextStyle(
                                    fontSize: 20, fontWeight: FontWeight.bold),
                              ),
                            ),
                          ],
                        ),
                        SizedBox(
                          height: 50,
                          child: Container(
                              // color: Colors.red,
                              ),
                        ),
                        Column(
                          children: [
                            Padding(
                              padding: const EdgeInsets.all(10.0),
                              child: Table(
                                  border: TableBorder.all(color: Colors.black),
                                  children: [
                                    TableRow(children: [
                                      TableCell(
                                          child: Center(child: Text('Date '))),
                                      TableCell(
                                          child: Center(child: Text('Time'))),
                                      TableCell(
                                          child: Center(
                                              child: Text('Temperature'))),
                                      TableCell(
                                          child:
                                              Center(child: Text('Humidity'))),
                                      TableCell(
                                          child: Center(
                                              child: Text('Fire Alarm'))),
                                    ]),
                                    TableRow(children: [
                                      TableCell(
                                        child: Center(
                                            child: Text(
                                          formattedDate,
                                        )),
                                      ),
                                      TableCell(
                                        child: Center(
                                          child: Text(formattedTime),
                                        ),
                                      ),
                                      TableCell(
                                          child: Center(
                                              child:
                                                  Center(child: Text('Temp')))),
                                      TableCell(
                                          child: Center(
                                              child: Center(
                                                  child: Text('Humidity')))),
                                      TableCell(
                                          child: Center(child: Text('None'))),
                                    ]),
                                    TableRow(children: [
                                      TableCell(
                                        child: Center(
                                            child: Text(
                                          formattedDate,
                                        )),
                                      ),
                                      TableCell(
                                        child: Center(
                                          child: Text(formattedTime),
                                        ),
                                      ),
                                      TableCell(
                                          child: Center(
                                              child:
                                                  Center(child: Text('Temp')))),
                                      TableCell(
                                          child: Center(
                                              child: Center(
                                                  child: Text('Humidity')))),
                                      TableCell(
                                          child: Center(child: Text('None'))),
                                    ]),
                                    TableRow(children: [
                                      TableCell(
                                        child: Center(
                                            child: Text(
                                          formattedDate,
                                        )),
                                      ),
                                      TableCell(
                                        child: Center(
                                          child: Text(formattedTime),
                                        ),
                                      ),
                                      TableCell(
                                          child: Center(
                                              child:
                                                  Center(child: Text('Temp')))),
                                      TableCell(
                                          child: Center(
                                              child: Center(
                                                  child: Text('Humidity')))),
                                      TableCell(
                                          child: Center(child: Text('None'))),
                                    ]),
                                  ]),
                            ),
                          ],
                        )
                      ],
                    ),
                  );
                } else {}
                return Container();
              },
              stream: dbRef.child("Data").onValue),
        ),
      ),
    );
  }

  Future<void> readData() async {
    dbRef.child("Data").once().then((DataSnapshot snapshot) {
      print(snapshot.value);
    });
  }
}
