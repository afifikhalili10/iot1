import 'dart:async';
import 'package:flutter/material.dart';
import 'package:firebase_database/firebase_database.dart';
//import 'package:flutter/services.dart';

import 'CircleProgress.dart';

class HumidSensor extends StatefulWidget {
  @override
  _HumidSensorState createState() => _HumidSensorState();
}

class _HumidSensorState extends State<HumidSensor>
    with SingleTickerProviderStateMixin {
  //
  final dbRef = FirebaseDatabase.instance.reference();
  //bool value = false;
  bool isLoading = false;

  //Animation
  AnimationController progressController;
  Animation<double> tempAnimation;
  Animation<double> humidityAnimation;

  @override

  //
  void initState() {
    super.initState();

    dbRef.child('Data').once().then((DataSnapshot snapshot) {
      double temp = snapshot.value['Temperature'];
      double humidity = snapshot.value['Humidity'];

      isLoading = true;
      _HumidSensorInit(temp, humidity);
    });
  }

  //

  _HumidSensorInit(double temp, double humid) {
    progressController = AnimationController(
        vsync: this, duration: Duration(milliseconds: 5000)); //5s

    tempAnimation =
        Tween<double>(begin: -50, end: temp).animate(progressController)
          ..addListener(() {
            setState(() {});
          });

    humidityAnimation =
        Tween<double>(begin: 0, end: humid).animate(progressController)
          ..addListener(() {
            setState(() {});
          });

    progressController.forward();
  }

  Widget build(BuildContext context) {
    return Scaffold(
      //Wallpaper Background

      body: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage("assets/images/bluelightGradient.jpg"),
            fit: BoxFit.cover,
          ),
        ),
        child: SafeArea(
          child: StreamBuilder(
              builder: (context, snapshot) {
                if (snapshot.hasData &&
                    !snapshot.hasError &&
                    snapshot.data.snapshot.value != null) {
                  //
                  return Center(
                    child: Column(
                      children: [
                        Padding(
                          padding: EdgeInsets.all(20),
                          child: Row(
                            //
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              //Name Area
                              Text(
                                "Humidity",
                                style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 20,
                                    fontWeight: FontWeight.bold),
                              ),

                              // Icon(Icons.settings)
                            ],
                          ),
                        ),

                        // Box Empty Space
                        SizedBox(
                          height: 50,
                          child: Container(
                              // color: Colors.red,
                              ),
                        ),

                        Column(
                          children: [
                            Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Text(
                                "Humidity",
                                style: TextStyle(
                                    fontSize: 20, fontWeight: FontWeight.bold),
                              ),
                            ),
                            CustomPaint(
                              foregroundPainter: CircleProgress(
                                  humidityAnimation.value, false),
                              child: Container(
                                width: 200,
                                height: 200,
                                child: Center(
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: <Widget>[
                                      Text('Humidity'),
                                      Text(
                                        '${humidityAnimation.value.toInt()}',
                                        style: TextStyle(
                                            fontSize: 50,
                                            fontWeight: FontWeight.bold),
                                      ),
                                      Text(
                                        '%',
                                        style: TextStyle(
                                            fontSize: 20,
                                            fontWeight: FontWeight.bold),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  );
                } else {}
                return Container();
              },
              stream: dbRef.child("Data").onValue),
        ),
      ),
    );
  }

  Future<void> readData() async {
    dbRef.child("Data").once().then((DataSnapshot snapshot) {
      print(snapshot.value);
    });
  }
}
