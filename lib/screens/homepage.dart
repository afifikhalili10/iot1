import 'package:flutter/material.dart';
import 'package:iot_home_app/details/Introduction.dart';
import 'package:iot_home_app/details/Tutorial.dart';
import 'package:iot_home_app/shared/screenTitle.dart';
//import 'package:iot_home_app/shared/columnList.dart';

//
//import 'package:iot_home_app/screens/ColumnList2.dart';
//import 'package:iot_home_app/details/Introduction.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  //utk dibawah scroolBar

  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        //BackGround Wallpaper
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage("assets/images/bluelightGradient.jpg"),
            fit: BoxFit.cover,
          ),
        ),

        //jarak antara side phone padding
        padding: EdgeInsets.all(11),

        //For Column from top to bottom
        child: Column(
          // Name Tittle App
          crossAxisAlignment: CrossAxisAlignment.center,

          //related to Title space
          children: <Widget>[
            SizedBox(height: 30),
            SizedBox(
              height: 120,
              child: ScreenTitle(text: 'IOT Smart Home System'),
            ),
            Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Container(
                  height: 120,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    image: DecorationImage(
                      image: AssetImage("assets/images/introduction.png"),
                      fit: BoxFit.fitHeight,
                      alignment: Alignment(0.9, 0.1),
                    ),

                    color: Colors.red[200],
                    //boleh letak gambar utk gantikan background hijau
                  ),
                  child: ListTile(
                      title: Text('Introduction',
                          style: TextStyle(fontSize: 30, color: Colors.white)),
                      contentPadding: EdgeInsets.all(5),
                      onTap: () {
                        Navigator.push(
                            context,
                            //trip => list
                            MaterialPageRoute(
                                builder: (context) => Introduction()));
                      }),
                ),
                SizedBox(
                  height: 10,
                ),

                //utk Tutorial
                Container(
                  child: ListTile(
                      title: Text('Tutorial',
                          style: TextStyle(fontSize: 30, color: Colors.white)),
                      contentPadding: EdgeInsets.all(5),
                      onTap: () {
                        Navigator.push(
                            context,
                            //trip => list
                            MaterialPageRoute(
                                builder: (context) => Tutorial()));
                      }),

                  //utk size container 4 subtopic
                  height: 120,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    image: DecorationImage(
                      image: AssetImage("assets/images/tutorial.png"),
                      fit: BoxFit.fitHeight,
                      alignment: Alignment(0.9, 0.1),
                    ),

                    color: Colors.green[
                        200], //boleh letak gambar utk gantikan background hijau
                  ),
                ),
                //Jarak Spacing
                SizedBox(
                  height: 10,
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }

//Buat Introduction and Tutorial App ini dan design skt..
}
